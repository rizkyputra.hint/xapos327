package com.xsisacademy.pos.xsisacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long>{
	@Query("SELECT max(o.id) FROM OrderHeader o")
	public Long findByMaxId();
}

package com.xsisacademy.pos.xsisacademy.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Long>{
	
	@Query(value = "SELECT * FROM category where is_active = true order by category_name", nativeQuery = true)
//	@Query(value = "SELECT c FROM Category c where c.isActive = true") //Java Class
	List<Category> findByCategories();

	//PAGING ASC
	@Query(value = "SELECT * FROM category where LOWER(category_name) LIKE LOWER (CONCAT('%',?1,'%')) AND is_active = ?2 ORDER BY category_name ASC", nativeQuery = true)
	Page<Category> findByIsActive(String Keyword, Boolean IsActive, Pageable page);
	
	//PAGING DESC 
	@Query(value = "SELECT * FROM category where LOWER(category_name) like lower(concat('%',?1,'%')) and is_active = ?2 ORDER BY category_name DESC", nativeQuery = true)
	Page<Category> findByIsActiveDESC(String Keyword, Boolean IsActive, Pageable page);

	@Query(value="select * from category where is_active = true and category_name = ?1 limit 1",nativeQuery = true)
    Category findByIdName(String categoryName);

    @Query(value="select * from category where is_active = true and category_name = ?1 and id != ?2 limit 1",nativeQuery = true)
    Category findByIdNameForEdit(String categoryName,Long id);
}

package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Variant;

public interface VariantRepository extends JpaRepository<Variant,Long> {
	@Query(value = "SELECT * FROM variant where is_active = true order by variant_name", nativeQuery = true)
	List<Variant> findByVariants();
	
	@Query(value = "SELECT v FROM Variant v where v.isActive = true and v.categoryId = ?1")
	List<Variant> findByCategoryId(Long categoryId);
}

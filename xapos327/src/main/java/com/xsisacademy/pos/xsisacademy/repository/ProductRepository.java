package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	List<Product> findByisActive(Boolean isActive);
	
	//Contoh Pakai JAVACLASS
	//@Query(value = "select p from Product p where p.isActive = ?1 and p.productInitial = ?2 order by p.productInitial")
	//List<Product> findByisActive(Boolean isActive, String productInitial);
}
